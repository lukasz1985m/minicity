/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modes;

import input.MouseHandler;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import minicity.GameMode;
import static minicity.Globals.*;
import minicity.Sprite;
import terrain.Tile;
import terrain.TileRect;
import utils.Vector3D;
import world.Size;
import world.Zone;
import world.ZoneType;
/**
 *
 * @author lukasz
 */
public class ZoningMode extends GameMode implements MouseHandler
{

    ZoneType zoning_type = ZoneType.Residental3X3;
    Sprite   zoning_sprite;
    
    
    @Override
    public void enable() {
        super.enable(); //To change body of generated methods, choose Tools | Templates.
        input.addMouseHandler(this);
        
        // Remove previous zoning sprite if possible;
        if (this.zoning_sprite != null) {
            world.removeSprite(this.zoning_sprite); 
        }
        
        this.zoning_sprite = zoning_type.getZoningSprite();
        world.addSprite(this.zoning_sprite);
    }

    @Override
    public void disable() {
        super.disable(); //To change body of generated methods, choose Tools | Templates.
        input.removeMouseHandler(this);
        
        world.removeSprite(this.zoning_sprite);
        this.zoning_sprite = null;
    }

    
    public void setType(ZoneType type) {
        this.zoning_type = type;
    }
    
    @Override
    public boolean mouseMoved(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        if (tile != null) {
            this.positionZoneSprite(tile);
        }
        
        return false;
    }

    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    private void positionZoneSprite(Tile tile) {
        if (tile != null) {
            Vector3D tile_loc = tile.getLocation();
            Size zone_size = this.zoning_type.getSize();
            
            int w = this.zoning_type.size.size_x;
            int h = this.zoning_type.size.size_y;
            
            TileRect rect = terrain.getTileRect(tile.x, tile.y, tile.x + w, tile.y + h);
            
            if (rect != null  && !rect.containsRoads() && !rect.containsWater()) {
                this.zoning_sprite.setLocation(
                    tile_loc.x + zone_size.offset_x_sprite, 
                    tile_loc.y + zone_size.offset_y_sprite , 
                    0
                );
            }
        }
    }
    
    @Override
    public boolean mouseReleased(MouseEvent e) {
        
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        if (tile != null) {
            this.positionZoneSprite(tile);
            Vector3D loc  = this.zoning_sprite.getLocation();
            Zone     zone = this.zoning_type.createZone();

            int w = this.zoning_type.size.size_x;
            int h = this.zoning_type.size.size_y;

            TileRect rect = terrain.getTileRect(
                    tile.x + this.zoning_type.size.offset_x,
                    tile.y + this.zoning_type.size.offset_y, 
                    tile.x + w + this.zoning_type.size.offset_x, 
                    tile.y + h + this.zoning_type.size.offset_y);

            
            if (rect.isEmpty() && !rect.containsRoads() && !rect.containsWater()) {
                rect.addObject(zone);
                zone.setLocation(loc);
                zone.setPlaced(true);
                world.addSprite(zone);
            }

        }
        
        return false;
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }    
}
