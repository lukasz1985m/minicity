/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modes;

import input.MouseHandler;
import world.Building;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import minicity.GameMode;
import terrain.Grid;
import terrain.TileRect;
import terrain.Tile;
import static minicity.Globals.game;
import static minicity.Globals.input;
import static minicity.Globals.land;
import static minicity.Globals.select_mode;
import static minicity.Globals.status_bar;
import static minicity.Globals.terrain;
import static minicity.Globals.viewport;
import static minicity.Globals.world;
import world.Size;

/**
 *
 * @author lukasz
 */
public class PlacementMode extends GameMode implements MouseHandler
{
    
    ArrayList<Building> queue = new ArrayList<>();

    @Override
    public void enable() {
        super.enable();
        world.addSprite(this.queue.get(0));
        input.addMouseHandler(this);
    }

    @Override
    public void disable() {
        super.disable(); //To change body of generated methods, choose Tools | Templates.
        if (!this.queue.isEmpty()) {
            world.removeSprite(this.queue.get(0));
        }
        input.removeMouseHandler(this);
    }
    
    
    public void enqueue(Building building) {
        this.queue.add(building);
    }
    
    @Override
    public boolean mouseMoved(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        if (tile != null) {
            
            Building building = this.queue.get(0);
            if (building.zone.type.size == Size.SIZE3X3) {
                building.setLocation(tile.x, tile.y, 0);
            }
            else {
                building.setLocation(tile.x + 0.5f, tile.y + 0.5f, 0);
            }
        }
        
        
        return false;
    }

    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        Building building = this.queue.get(0);
        
        TileRect rect = null;
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        
        if (tile == null) {
            return false;
        }
        try {
            
            Size size = building.zone.type.size;
            if (size == Size.SIZE2X2) {
                rect = terrain.getTileRect(tile.x, tile.y , tile.x + 2, tile.y + 2);
            }
            else if (size == Size.SIZE3X3) {
                rect = terrain.getTileRect(tile.x - 1, tile.y -1 , tile.x + 2, tile.y + 2);
            }

            if (rect.isEmpty() && !rect.containsRoads() && !rect.containsWater()) {
                if (this.queue.size() > 0) {
                    Building placed = building.copy();
                    
                    if (placed.zone.type.size == Size.SIZE2X2) {
                        placed.setLocation(tile.x + 0.5f, tile.y + 0.5f, 0);
                    }
                    if (placed.zone.type.size == Size.SIZE3X3) {
                        placed.setLocation(tile.x, tile.y, 0);
                    }
                    world.addSprite(placed);
                    world.removeSprite(this.queue.get(0));
                    if (this.queue.size() == 1) { // No more buildings to place
                        game.setMode(select_mode);
                    }
                    this.queue.remove(0);
                    if (this.queue.size() > 0) {
                        world.addSprite(this.queue.get(0));
                    }
                    
                    rect.addObject(placed);
                    placed.setRect(rect);
                    placed.onPlace();
                    
                }
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            status_bar.displayMessage("Cannot place in this position!");
        }
        
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }
    
    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }
}
