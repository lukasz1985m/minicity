/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modes;

import input.MouseHandler;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import minicity.GameMode;
import static minicity.Globals.*;
import minicity.Sprite;
import terrain.Tile;
import terrain.TileRect;
import utils.Assets;
import terrain.Tree;
/**
 *
 * @author lukasz
 */
public class CutTreesMode extends GameMode implements MouseHandler
{

    @Override
    public void enable() {
        super.enable(); //To change body of generated methods, choose Tools | Templates.
        input.addMouseHandler(this);
    }

    @Override
    public void disable() {
        super.disable(); //To change body of generated methods, choose Tools | Templates.
        input.removeMouseHandler(this);
    }

    Tile start_tile;
    Tile end_tile;
    ArrayList<Sprite> highlights = new ArrayList<>();
    
    
    @Override
    public boolean mouseMoved(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        this.start_tile = viewport.pickTile(e.getX(), e.getY());
        return false;
    }
    
    @Override
    public boolean mouseDragged(MouseEvent e) {
        this.end_tile = viewport.pickTile(e.getX(), e.getY());
        this.highlightToEnd(this.end_tile);
        return false;
    }

    
    private void highlightToEnd(Tile end) {
        this.clearHighlight();
        
        if (this.start_tile != null) {
            
            
            TileRect rect = terrain.getTileRect(
                    this.start_tile.x, this.start_tile.y,
                    end.x, end.y
            );
            
            for (Tile tile : rect) {
                Sprite highlight = new Sprite(Assets.getImage("/zoning/destroy.png"));
                highlight.setLocation(tile.x, tile.y, 0);
                this.highlights.add(highlight);
                world.addSprite(highlight);
            }
            
        }
    }
    
    private void clearHighlight() {
        for (Sprite highlight : this.highlights) {
            world.removeSprite(highlight);
        }
        this.highlights.clear();
        
    }
    
    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }


    @Override
    public boolean mouseReleased(MouseEvent e) {
        
        Tile end = viewport.pickTile(e.getX(), e.getY());
        
        this.highlightToEnd(end);
        
        TileRect rect = terrain.getTileRect(
            this.start_tile.x, this.start_tile.y,
            end.x, end.y
        );
        
        ArrayList<Object> trees = rect.getObjects(Tree.class);
        
        for (Object tree_obj : trees) {
            Tree tree = (Tree)tree_obj;
            world.removeSprite(tree);
        }
        
        rect.removeObjects(Tree.class);
        
        this.clearHighlight();
        return false;
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }
    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }
    
    
    
}
