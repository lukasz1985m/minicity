/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modes;

import input.MouseHandler;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import minicity.GameMode;
import minicity.RoadOperations;
import terrain.Tile;
import static minicity.Globals.*;
import minicity.Sprite;
import utils.Assets;
import utils.Vector3D;

/**
 *
 * @author lukasz
 */
public class RemoveRoadMode extends GameMode implements MouseHandler
{

    private Sprite delete;
    
    @Override
    public void enable() {
        super.enable(); //To change body of generated methods, choose Tools | Templates.
        input.addMouseHandler(this);
        this.delete = Assets.getSprite("/delete.png");
        world.addSprite(this.delete);
    }

    @Override
    public void disable() {
        super.disable(); //To change body of generated methods, choose Tools | Templates.
        input.removeMouseHandler(this);
        world.removeSprite(this.delete);
        this.delete = null;
    }
    
    @Override
    public boolean mouseMoved(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        if (tile != null) {
            Vector3D location = tile.getLocation();
            this.delete.setLocation(location);
        }
        return false;
    }

    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        RoadOperations.removeRoad(tile);
        
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }

   
    
    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }
    
}
