/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modes;

import input.MouseHandler;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import minicity.GameMode;
import terrain.Tile;
import static minicity.Globals.*;

/**
 *
 * @author lukasz
 */
public class SelectMode extends GameMode implements MouseHandler
{

    @Override
    public void enable() {
        input.addMouseHandler(this);
    }

    @Override
    public void disable() {
        input.removeMouseHandler(this);
    }

    @Override
    public boolean mouseMoved(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
       return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        tile.setVisible(false);
        
        return false;
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }
    
    
    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }
    
}
