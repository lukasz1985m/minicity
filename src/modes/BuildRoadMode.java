/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modes;

import input.MouseHandler;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import minicity.GameMode;
import minicity.RoadOperations;
import terrain.Grid;
import terrain.RoadTile;
import terrain.Tile;
import utils.Assets;
import static minicity.Globals.*;
import minicity.Sprite;
import utils.Vector3D;

/**
 *
 * @author lukasz
 */
public class BuildRoadMode extends GameMode implements MouseHandler
{
    
    Sprite highlight;
    
    @Override
    public void enable() {
        super.enable(); //To change body of generated methods, choose Tools | Templates.
        input.addMouseHandler(this);
        this.highlight = Assets.getSprite("/highlight.png");
        world.addSprite(this.highlight);
    }

    @Override
    public void disable() {
        super.disable(); //To change body of generated methods, choose Tools | Templates.
        input.removeMouseHandler(this);
        world.removeSprite(this.highlight);
        this.highlight = null;
    }

    @Override
    public boolean mouseMoved(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        if (tile != null) {
            Vector3D location = tile.getLocation();
            this.highlight.setLocation(location);
        }
        return false;
    }

    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }
    
    
    

    @Override
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        Tile tile = viewport.pickTile(e.getX(), e.getY());
        if (tile != null && tile.isGround() && tile.isEmpty()) { 
            RoadOperations.buildRoad(tile);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }
    
    
    
    
}
