/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import java.awt.event.KeyEvent;

/**
 *
 * @author lukasz
 */
public interface KeyHandler {
    public boolean keyPressed(KeyEvent e);
    public boolean keyTyped(KeyEvent e);
    public boolean keyReleased(KeyEvent e);
}
