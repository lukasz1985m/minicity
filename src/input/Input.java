/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 *
 * @author lukasz
 */
public class Input implements MouseMotionListener, MouseListener, MouseWheelListener, KeyListener {

    CopyOnWriteArrayList<MouseHandler> mouse_handlers = new CopyOnWriteArrayList<>();
    CopyOnWriteArrayList<KeyHandler>   key_handlers   = new CopyOnWriteArrayList<>();
    
    
    public void addMouseHandler(MouseHandler handler) {
        this.mouse_handlers.add(handler);
    }
    
    public void addKeyHandler(KeyHandler handler) {
        this.key_handlers.add(handler);
    }
    
    public void removeMouseHandler(MouseHandler handler) {
        this.mouse_handlers.remove(handler);
    }
    
    public void removeKeyHandler(KeyHandler handler) {
        this.key_handlers.remove(handler);
    }
    
    
    @Override
    public void mouseDragged(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseDragged(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseMoved(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseClicked(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mousePressed(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseReleased(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseEntered(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseExited(e);
            if (consumed) {
                break;
            }
        }
    }
    
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        for (MouseHandler mouse_handler : this.mouse_handlers) {
            boolean consumed = mouse_handler.mouseWheelMoved(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        for (KeyHandler key_handler : this.key_handlers) {
            boolean consumed = key_handler.keyTyped(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        for (KeyHandler key_handler : this.key_handlers) {
            boolean consumed = key_handler.keyPressed(e);
            if (consumed) {
                break;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        for (KeyHandler key_handler : this.key_handlers) {
            boolean consumed = key_handler.keyReleased(e);
            if (consumed) {
                break;
            }
        }
    }

    
    
}
