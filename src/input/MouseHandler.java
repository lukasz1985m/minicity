/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package input;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

/**
 *
 * @author lukasz
 */
public interface MouseHandler {
    boolean mouseMoved(MouseEvent e);
    boolean mouseDragged(MouseEvent e);
    boolean mouseClicked(MouseEvent e);
    boolean mousePressed(MouseEvent e);
    boolean mouseReleased(MouseEvent e);
    boolean mouseEntered(MouseEvent e);
    boolean mouseExited(MouseEvent e);
    boolean mouseWheelMoved(MouseWheelEvent e);
}
