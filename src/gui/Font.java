/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * A bitmap font representation.
 * @author lukasz
 */
public class Font 
{
    public String name;
    public int size;
    
    public BufferedImage atlas;
    
    HashMap<Character, Glyph> glyphs = new HashMap<>();
    
    public void addGlyph(char character, Glyph glyph){
        this.glyphs.put(character, glyph);
    }
    
    Glyph getGlyph(char chr) {
        return this.glyphs.get(chr);
    }
    
    boolean containsGlypgh(char character) {
        return this.glyphs.containsKey(character);
    }
    
}
