/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import utils.IntVector2D;

/**
 *
 * @author lukasz
 */
public class Container extends Widget
{
    private ArrayList<Widget> widgets = new ArrayList<>();
    
    private Widget background;
    
    public void add(Widget w) {
        this.widgets.add(w);
    }
    
    public void remove(Widget w) {
        this.widgets.remove(w);
    }

    
    public void setBackground(Widget w) {
        this.background = w;
    }
    
    @Override
    public void setDimensions(int x, int y) {
        super.setDimensions(x, y); //To change body of generated methods, choose Tools | Templates.
        if (this.background !=null) {
            this.background.setDimensions(x,y);
        }
    }

    @Override
    public void draw(Graphics2D g2d) {
        
        if (this.background !=null) {
            this.background.draw(g2d);
        }
        
        for (Widget widget : this.widgets) {
            widget.draw(g2d);
        }
        
    }

    public ArrayList<Widget> getWidgets() {
        return this.widgets;
    }

    @Override
    public void setPosition(int x, int y) {
        // Move the children with this container:
        IntVector2D old_pos = this.getPosition();
        int dx = x - old_pos.x;
        int dy = y - old_pos.y;
        
        for (Widget widget : this.widgets) {
            IntVector2D widget_pos = widget.getPosition();
            
            widget_pos.x += dx;
            widget_pos.y += dy;
            
            widget.setPosition(widget_pos);
        }
        
        super.setPosition(x, y);
        
        if (this.background != null) {
            this.background.setPosition(x, y);
        }
    }

    
    

    @Override
    public Widget getHoverWidget(MouseEvent e) {
        Widget hover = null;
        
        for (Widget widget : this.widgets) {
            
            Widget hover_widget = widget.getHoverWidget(e);
            if (hover_widget != null) {
                hover = hover_widget;
            }
        }
        
        if (hover == null ) {
            return super.getHoverWidget(e);
        }
        
        return hover;
        
    }
    
    
    
}
