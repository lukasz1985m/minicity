/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;
import input.KeyHandler;
import input.MouseHandler;
import java.awt.event.MouseWheelEvent;

/**
 *
 * @author lukasz
 */
public class Gui implements MouseHandler, KeyHandler
{
    CopyOnWriteArrayList<Widget> widgets = new CopyOnWriteArrayList<>();

    Widget hover = null;
    Widget focus = null;
    
    public void add(Widget w) {
        this.widgets.add(w);
    }
    
    public void remove(Widget w) {
        this.widgets.remove(w);
    }
    
    public void draw(Graphics2D g2d){
        for (Widget widget : this.widgets) {
            widget.draw(g2d);
        }
    };

    @Override
    public boolean mouseMoved(MouseEvent e) {
        Widget old_hover = this.hover;
        Widget new_hover = null;
        
        for (Widget widget : this.widgets) {
            Widget hover = widget.getHoverWidget(e);
            if (hover != null) {
                new_hover = hover;
            }
        }
        
        if (old_hover != new_hover) {
            if (old_hover != null) {
                System.out.println("Hover from widget: " + old_hover );
                old_hover.onMouseOut(e);
            }
            
            if (new_hover != null) {
                System.out.println("Hover to widget: " + new_hover );
                new_hover.onMouseOver(e);
            }
            
            this.hover = new_hover;
        }
        
        return false;
    }

    
    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;

    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        if (this.hover != null) {
            this.focus = this.hover;
            this.focus.mouseClicked(e);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        if (this.hover != null) {
            this.hover.mousePressed(e);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        if (this.hover != null) {
            this.hover.mouseReleased(e);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        if (this.focus != null) {
            return this.focus.keyPressed(e);
        } else {
            return false;
        }
        
    }

    @Override
    public boolean keyTyped(KeyEvent e) {
        if (this.focus != null) {
            return this.focus.keyTyped(e);
        } else {
            return false;
        }
    }

    @Override
    public boolean keyReleased(KeyEvent e) {
        if (this.focus != null) {
            return this.focus.keyReleased(e);
        } else {
            return false;
        }
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        return false;
    }
    
    
    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        return false;
    }
}
