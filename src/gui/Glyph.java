/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author lukasz
 */
public class Glyph 
{
    public Glyph(char chr, int x, int y, int width, int height, int x_offset, int y_offset, int advance) {
        this.chr = chr;
        
        this.x = x;
        this.y = y;
        
        this.width  = width;
        this.height = height;
        
        this.x_offset = x_offset;
        this.y_offset = y_offset;
        
        this.advance = advance;
    }
    
    char chr;
    int x, y; 
    int width, height;
    int x_offset, y_offset;
    int advance;
}
