/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import utils.IntVector2D;

/**
 *
 * @author lukasz
 */
public class TextField extends Text
{
    public TextField(String contents) {
        super(contents);
    }

    int caret_index = 0;
    IntVector2D caret_position = new IntVector2D();
    
    @Override
    public boolean keyTyped(KeyEvent e) {
    
        char character = e.getKeyChar();
        if (this.font.containsGlypgh(character)) {
            String before = this.contents.substring(0, this.caret_index);
            String after = this.contents.substring(this.caret_index, this.contents.length());
            before += character;
            String new_contents = before + after;

            this.setContents(new_contents);

            this.caret_index += 1;
            this.updateCaret();
            this.recalculateDimensions();
        } 
        return true;
    }

    @Override
    public boolean keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        
        System.out.println(code);
        
        if (code == 37) { // Left on kbd
            if (this.caret_index > 0) {
                this.caret_index -= 1;
                this.updateCaret();
            }
        }
        if (code == 39) { // Right on kbd
            if (this.caret_index < this.contents.length()) {
                this.caret_index += 1;
                this.updateCaret();
            }
        }
        if (code == 8) { // Backspace
            if (this.caret_index > 0 && this.contents.length() > 0) {
                String before = this.contents.substring(0, this.caret_index - 1);
                String after = this.contents.substring(this.caret_index, this.contents.length());
                String new_contents = before + after;
                this.setContents(new_contents);
                this.caret_index -= 1;
                this.updateCaret();
            }
        }
        
        if (code == 127) { // Delete
            if (this.caret_index < this.contents.length()) {
                String before = this.contents.substring(0, this.caret_index);
                String after = this.contents.substring(this.caret_index + 1, this.contents.length());
                String new_contents = before + after;
                this.setContents(new_contents);
    //            this.caret_index -= 1;
                this.updateCaret();
            }
        }
        
        this.recalculateDimensions();
        return true;
    }
    
    
    

    @Override
    public boolean mouseClicked(MouseEvent e) {
        int event_x = e.getX();
        int event_y = e.getY();
        
        IntVector2D position   = this.getPosition();
        IntVector2D dimensions = this.getDimensions();
        
        System.out.println("clicked textfield");
        
        if (    event_y < position.y || 
                event_y > position.y + dimensions.y || 
                event_x < position.x || 
                event_x > position.x + dimensions.x) {
            return false;
        }
        
        int x = position.x;
        
        int index = 0;
        
        for (int i = 0; i < this.contents.length(); i++) {
            char chr = this.contents.charAt(i);
            Glyph glyph = this.font.getGlyph(chr);
            if (event_x >= x && event_x < x + glyph.advance) {
                break;
            }
            x += glyph.advance;
            index++;
        }
        
        this.caret_index = index;
        this.caret_position.x = x;
        this.caret_position.y = position.y;
        
        return true;
    }

    private void updateCaret() {
        IntVector2D position   = this.getPosition();
        int x = position.x;
        
        for (int i = 0; i < this.caret_index; i++) {
            char chr = this.contents.charAt(i);
            Glyph glyph = this.font.getGlyph(chr);
            x += glyph.advance;
        }
        this.caret_position.x = x;
        this.caret_position.y = position.y;
    }

    @Override
    public void setPosition(int x, int y) {
        IntVector2D position = this.getPosition();
        int dx = x - position.x;
        this.caret_position.x += dx;
        this.caret_position.y = y;
        
        super.setPosition(x, y); 
    }
    
    
    
    @Override
    public void draw(Graphics2D g2d) {
        super.draw(g2d); 
        
        g2d.setColor(Color.black);
        g2d.fillRect(
            this.caret_position.x, this.caret_position.y, 
            2, font.size
        );
        
        
    }
    
    
    
}
