/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import utils.Assets;
import utils.IntVector2D;

/**
 *
 * @author lukasz
 */
public class Button extends Widget
{

    public Button(String path) {
        this(Assets.getImage(path));
    }
    
    
    public Button(BufferedImage img) {
        this.img = img;
        this.setDimensions(img.getWidth(), img.getHeight());
    }

    private final BufferedImage img;
    boolean pressed = false;

    
    @Override
    public boolean mousePressed(MouseEvent e) {
        this.pressed = true;
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        this.pressed = false;
        return false;
    }
    
    
    
    
    @Override
    public void draw(Graphics2D g2d) {
        IntVector2D position = this.getPosition();
        IntVector2D dimensions = this.getDimensions();
        g2d.drawImage(this.img, position.x, position.y, null);
        if (this.pressed) {
            g2d.setColor(Color.BLACK);
            g2d.drawRect(position.x, position.y, dimensions.x, dimensions.y);
        }
    }
    
}
