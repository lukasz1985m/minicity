/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Graphics2D;
import utils.IntVector2D;

/**
 *
 * @author lukasz
 */
public class Box extends Widget
{
    
    public Box() {}
    
    public Box(Color color) {
        this.color = color;
    }
    
    public Box(int r, int g, int b) {
        this.color = new Color(r,g,b);
    }
    
    Color color = new Color(0, 0, 0);

    public void setColor(Color color) {
        this.color = new Color(color.getRGB());
    }
    
    public void setColor(int red, int green, int blue, int alpha) {
        this.color = new Color(red, green, blue, alpha);
    }
    
    @Override
    public void draw(Graphics2D g2d) {
        g2d.setColor(this.color);
        
        IntVector2D position = this.getPosition();
        IntVector2D dimensions = this.getDimensions();
        
        g2d.fillRect(position.x, position.y, dimensions.x, dimensions.y);
    }

    @Override
    public String toString() {
        return String.format("Box: %d %d %d", this.color.getRed(), this.color.getGreen(), this.color.getBlue());
    }
    
    
    
}
