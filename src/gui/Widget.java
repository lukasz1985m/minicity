/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import utils.IntVector2D;
import utils.Vector2D;

/**
 *
 * @author lukasz
 */
public abstract class Widget {
    
    private IntVector2D position   = new IntVector2D();
    private IntVector2D dimensions = new IntVector2D();
    
    final public void setDimensions(IntVector2D dimensions) {
        this.setDimensions(dimensions.x, dimensions.y);
    }
    
    public void setDimensions(int x, int y) {
        this.dimensions.set(x,y);
    }

    final public IntVector2D getDimensions() {
        return new IntVector2D(this.dimensions);
    }
    
    
    

    final public void setPosition(IntVector2D position) {
        this.setPosition(position.x, position.y);
    }

    public void setPosition(int x, int y) {
        this.position.set(x, y);
    }

    final public IntVector2D getPosition() {
        return new IntVector2D(this.position);
    }
    
    
    
    
    boolean containsMouse(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        
        int x_start = this.position.x;
        int x_end   = this.position.x + this.dimensions.x;
        
        int y_start = this.position.y;
        int y_end   = this.position.y + this.dimensions.y;
        
        if (x >= x_start && x <= x_end && y >= y_start && y <= y_end) {
            return true;
        }
        else {
            return false;
        }
        
    }
    
    public Widget getHoverWidget(MouseEvent e) {
        if (this.containsMouse(e)) {
            return this;
        } else {
            return null;
        }
    }
    
    public void onMouseOver(MouseEvent e)  {}
    public void onMouseOut(MouseEvent e) {}

    public boolean mousePressed(MouseEvent e) { return false; }
    public boolean mouseClicked(MouseEvent e) { return false; }
    public boolean mouseReleased(MouseEvent e) { return false; }
    
    public boolean keyPressed(KeyEvent e) { return false; }
    public boolean keyTyped(KeyEvent e) { return false; }
    public boolean keyReleased(KeyEvent e) { return false; }
    
    public abstract void draw(Graphics2D g2d);

}
