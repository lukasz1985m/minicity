/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Graphics2D;
import utils.Assets;
import utils.IntVector2D;

/**
 *
 * @author lukasz
 */
public class Text extends Widget
{
    
    public static Font default_font = Assets.loadFont("/fonts/arial.fnt");
    
    public Text(String contents) {
        this.contents = contents;
        this.recalculateDimensions();
    }
    
    protected Font font = default_font;
    
    protected String contents;

    public void setFont(Font font) {
        this.font = font;
        this.recalculateDimensions();
    }

    public void setContents(String contents) {
        this.contents = contents;
        this.recalculateDimensions();
    }

    public String getContents() {
        return this.contents;
    }
    
    
    
    @Override
    public void draw(Graphics2D g2d) {
        IntVector2D position = this.getPosition();
        int x = position.x;
        int y = position.y;
        
        String contents = this.getContents();
        
        for (int i = 0; i < contents.length(); i++) {
            char chr = contents.charAt(i);
            Glyph glyph = this.font.getGlyph(chr);
            
            
            g2d.drawImage(this.font.atlas, 
                    x + glyph.x_offset, y + glyph.y_offset,
                    x + glyph.width + glyph.x_offset, y + glyph.height + glyph.y_offset, 
                    glyph.x, glyph.y, glyph.x + glyph.width, glyph.y + glyph.height, null);
            
            x += glyph.advance;
            
        }
    }
        
        
    protected void recalculateDimensions(){
        int height = this.font.size;
        int width = 0;
        
        String contents = this.getContents();
        
        for (int i = 0; i < contents.length(); i++) {
            char chr = contents.charAt(i);
            Glyph glyph = this.font.getGlyph(chr);
            width += glyph.advance;
        }
        this.setDimensions(width, height);
    }

}
