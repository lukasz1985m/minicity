/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import static minicity.Globals.viewport;
import utils.Vector2D;

/**
 *
 * @author lukasz
 */
public class GroundTile extends Tile
{
    
    public GroundTile(BufferedImage image) {
        super(image);
    }

    @Override
    public void draw(Graphics2D g2d) {
        super.draw(g2d); //To change body of generated methods, choose Tools | Templates.
        if (!this.isEmpty()) {
            Vector2D projected = viewport.project(this.getLocation());
//            g2d.fillRect((int)projected.x - 2, (int)projected.y - 2, 4, 4);
        }
    }
    
    
    
}
