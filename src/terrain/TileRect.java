/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import java.util.ArrayList;

/**
 *
 * @author lukasz
 */
public class TileRect extends ArrayList<Tile>
{

    public TileRect(int start_x, int start_y, int end_x, int end_y) {
        this.start_x = start_x;
        this.start_y = start_y;
        this.end_x = end_x;
        this.end_y = end_y;
    }
    
    public int start_x, start_y, end_x, end_y;
    
    
    public boolean isEmpty() {
        boolean empty = true;
        
        for (Tile tile : this) {
            if (!tile.isEmpty()) {
                empty = false;
                break;
            }
        }
        
        return empty;
    }
    
    public void addObject(Object obj) {
        for (Tile tile : this) {
            tile.addObject(obj);
        }
    }
    
    public void removeObjects(Class cls) {
        for (Tile tile : this) {
            tile.removeObject(cls);
        }
    }
    
    public ArrayList<Object> getObjects(Class cls){
        ArrayList<Object> objects = new ArrayList<>();
        for (Tile tile : this) {
            Object object = tile.getObject(cls);
            if (object != null) {
                objects.add(object);
            }
        }
        return objects;
    }
    
    public boolean containsRoads() {
        boolean contains = false;
        for (Tile tile : this) {
            if (tile instanceof RoadTile) {
                contains = true;
                break;
            }
        }
        return contains;
    }
    
    public boolean containsWater(){
        boolean contains = false;
        for (Tile tile : this) {
            if (tile instanceof WaterTile) {
                contains = true;
                break;
            }
        }
        return contains;
    }
    
    
    
}
