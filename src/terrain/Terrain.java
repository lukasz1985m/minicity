/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import utils.Assets;
import utils.SimplexNoise;
import utils.Utils;
import utils.Vector3D;
import static minicity.Globals.land;
import minicity.Sprite;
import static minicity.Globals.world;
import world.OutOfMapException;

/**
 *
 * @author lukasz
 */
public class Terrain {
    
    public Terrain(Heightmap hm) {
        this.hm = hm;
        for (int y = 0; y < hm.height; y++) {
            for (int x = 0; x < hm.width; x++) {
                this.tiles.add(null);
            }
        }
        
    }
    
    ArrayList<Tile> tiles = new ArrayList<>();
    private final Heightmap hm;
    
    public void create() {
        for (int y = 0; y < this.hm.height - 1; y++) {
            for (int x = 0; x < this.hm.width - 1; x++) {
                // Place the tiles and obstacles like trees and mountains.
                int index = y * hm.width + x;
                
                
                
                
                int h_tl = this.hm.heights.get(index);
                int h_tr = this.hm.heights.get(index + 1);
                int h_bl = this.hm.heights.get(index + this.hm.width);
                int h_br = this.hm.heights.get(index + this.hm.width + 1);
                
                String file = "";
                Tile tile;
                BufferedImage img = null;
                if (h_tl == 0 || h_tr == 0 || h_bl == 0 || h_br == 0) { // Is this a water tile ?
                    file = String.format("/terrain/water_%d%d%d%d.png", h_tl, h_tr, h_bl, h_br);
                    try {
                        img = Assets.getImage(file);
                        tile = new WaterTile(img);
                    } catch (Exception e) {
                        System.out.println("Wrong file:" + file);
                        file = "/terrain/water_0000.png";
                        img =  Assets.getImage(file);
                        tile = new WaterTile(img);
                    }
                } else {
                    int land_variation = 1; // (int)Math.random() * 4;
                    file = String.format("/terrain/grass.png", land_variation);
                    img = Assets.getImage(file);
                    tile = new GroundTile(img);
                }
                
                this.tiles.set(index, tile);

                tile.x = x;
                tile.y = y;
                
                tile.setLocation(new Vector3D(x, y, 0));
                land.addSprite(tile);
                    
            }
        }
        
        
        // Place the mountains.
        /*
        for (int y = 1; y < hm.height - 2; y++) {
            for (int x = 1; x < this.hm.width -2; x++) {
                double rand = Math.random();
                
                if (rand < 0.02f) {
                    // Try to place a mountain
                    Grid grid = this.getGrid(x, y);
                    Grid grid_tl = this.getGrid(x-1 , y-1);
                    Grid grid_t  = this.getGrid(x, y-1);
                    Grid grid_tr = this.getGrid(x+1, y-1);
                    Grid grid_l  = this.getGrid(x-1, y);
                    Grid grid_r  = this.getGrid(x+1, y);
                    Grid grid_bl = this.getGrid(x-1, y+1);
                    Grid grid_b  = this.getGrid(x, y+1);
                    Grid grid_br = this.getGrid(x+1, y+1);
                    
                    boolean can_place = 
                        grid.isGround() && grid.isEmpty() && 
                        grid_tl.isGround() && grid_tl.isEmpty() &&
                        grid_t.isGround()  && grid_t.isEmpty() && 
                        grid_tr.isGround() && grid_tr.isEmpty() &&
                        grid_l.isGround() && grid_l.isEmpty() && 
                        grid_r.isGround() && grid_r.isEmpty() &&
                        grid_bl.isGround() && grid_bl.isEmpty() &&
                        grid_b.isGround() && grid_b.isEmpty() &&
                        grid_br.isGround() && grid_br.isEmpty();
                    
                    if (can_place) {
                        BufferedImage img = Assets.loadImage("/terrain/mountain3x3.png");
                        Mountain mountain = new Mountain(img);
                        mountain.setLocation(new Vector3D(x, y, 0));

                        grid.addObject(mountain);
                        grid_tl.addObject(mountain);
                        grid_t.addObject(mountain);
                        grid_tr.addObject(mountain);
                        grid_r.addObject(mountain);
                        grid_bl.addObject(mountain);
                        grid_b.addObject(mountain);
                        grid_br.addObject(mountain);
                        grid_bl.addObject(mountain);

                        world.addSprite(mountain);
                    }
                }
                
            }
        }
        
        // Generate trees:
        for (int y = 1; y < hm.height - 2; y++) {
            for (int x = 1; x < this.hm.width -2; x++) {
                
                double n = SimplexNoise.noise(x, y);
                
                Tile tile = this.getTile(x, y);
                
                if (n >0.7f && tile.isEmpty() && tile.isGround()) {
                    // Place a tree.
                    int variation = Utils.randint(1,3);
                    BufferedImage img = Assets.getImage(String.format("/trees/tree_%d.png", variation));
                    Tree tree = new Tree(img);
                    tree.setLocation(new Vector3D(x,y, 0));
                    
                    tile.addObject(tree);
                        
                    world.addSprite(tree);
                }
            }
        }
        */
    }
    
    public Tile getTile(int x, int y) {
        if (x < 0 || x >= this.hm.width - 1 || y < 0 || y >= this.hm.height - 1) {
            throw new OutOfMapException(x, y);
        }
        
        int index = y * this.hm.width + x;
        return this.tiles.get(index);
    }
    
    public void setTile(int x, int y, Tile tile) {
        int index = y * this.hm.width + x;
        this.tiles.set(index, tile);
    }
    
    public TileRect getTileRect(int start_x, int start_y, int end_x, int end_y) {
        TileRect rect = new TileRect(start_x, start_y, end_x, end_y);
        
        try {
            for (int y = start_y; y < end_y; y++) {
                for (int x = start_x; x < end_x; x++) {
                    // Place the tiles and obstacles like trees and mountains.
                    Tile tile = this.getTile(x, y);
                    rect.add(tile);
                }
            }
        } catch (OutOfMapException e) {
            return null;
        }
        return rect;
    }
    
    public static void main(String[] args) {
        SimplexNoiseHeightmap hm = new SimplexNoiseHeightmap(10, 10, 0);
        String hmstr = hm.toString();
        System.out.println(hmstr);
        Terrain terrain = new Terrain(hm);
        
    }
    
}
