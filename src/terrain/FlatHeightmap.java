/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import utils.SimplexNoise;

/**
 *
 * @author lukasz
 */
public class FlatHeightmap extends Heightmap
{
    public FlatHeightmap(int width, int height) {
        super(width, height);
    }
    
    
     private void generateHeights(int seed) {
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                int index = y * this.width + x;
                int height = 1;
                this.heights.set(index, height);
            }
        }
    }
}
