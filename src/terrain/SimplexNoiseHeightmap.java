/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import utils.SimplexNoise;

/**
 *
 * @author lukasz
 */
public class SimplexNoiseHeightmap extends Heightmap{

    public SimplexNoiseHeightmap(int width, int height, int seed) {
        this(width, height, seed, 2);
    }
    
    
    public SimplexNoiseHeightmap(int width, int height, int seed, int span) {
        super(width, height);
        this.span = span;
        this.generateHeights(seed);
    }
    
    private final int span;

    private void generateHeights(int seed) {
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                int index = y * this.width + x;
                int height = (int) (SimplexNoise.noise(x / (float)this.span + seed, y / (float)this.span + seed) * 10);
                if (height > -8) {
                    height = 1;
                } else {
                    height = 0;
                }
                this.heights.set(index, height);
            }
        }
    }
    
    
    
    @Override
    public String toString() {
        String str = "";
        
        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                int index = y * this.width + x;
                int height = this.heights.get(index);
                str += index + ":" +height + " ";
            }
            str +="\n";
        }
        
        return str;
    }
    
    public static void main(String[] args) {
        SimplexNoiseHeightmap hm = new SimplexNoiseHeightmap(10, 10, 0);
        
        String hmstr = hm.toString();
        
        System.out.println(hmstr);
        
                
    }
}
