/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terrain;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import utils.Vector2D;
import utils.Vector3D;
import static minicity.Globals.viewport;
import minicity.Sprite;
import minicity.Viewport;

/**
 *
 * @author lukasz
 */
public class Tile extends Sprite
{
    
    public Tile(BufferedImage image) {
        super(image);
    }
     
    public int x, y;   
    ArrayList<Object> objects = new ArrayList<>();
    
    public void addObject(Object o) {
        this.objects.add(o);
    }
    
    
    public void removeObject(Object o) {
        this.objects.remove(o);
    }
    
    public void removeObject(Class cls) {
        if (this.containsObject(cls)) {
            Object object = this.getObject(cls);
            this.objects.remove(object);
        }
    }
    
    public boolean containsObject(Class cls) {
        for (Object object : this.objects) {
            if (object.getClass().isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }
    
    public Object getObject(Class cls) {
        for (Object object : this.objects) {
            if (cls.isAssignableFrom(object.getClass())) {
                return object;
            }
        }
        return null;
    }
    
    
    public boolean isWater() {
        return this instanceof WaterTile;
    }
    
    public boolean isGround() {
        return this instanceof GroundTile;
    }

    public boolean isEmpty() {
        return this.objects.size() == 0;
    }
    
    public boolean isWalkable() {
        if (this.containsObject(Mountain.class) || !(this instanceof WaterTile)) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public String toString() {
        return String.format("Grid (%d, %d)", this.x, this.y);
    }
    
    
    
}
