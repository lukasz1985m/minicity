/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import world.behaviors.Behavior;
import java.awt.image.BufferedImage;
import terrain.Grid;
import minicity.AnimatedSprite;
import minicity.Sprite;
import minicity.Timer;
import terrain.Tile;
/**
 *
 * @author lukasz
 */
public abstract class Car extends Sprite
{
    
    public Car(BufferedImage west, BufferedImage east, BufferedImage north, BufferedImage south) {
        super(west);
        this.west  = west;
        this.east  = east;
        this.north = north;
        this.south = south;
    }

    private BufferedImage west;
    private BufferedImage east;
    private BufferedImage north;
    private BufferedImage south;
    
    public boolean idle = true;
    public Tile tile;    // The grid the car is on.
    
    public Behavior behavior;
    
    
    @Override
    public void update(Timer timer) {
        super.update(timer); //To change body of generated methods, choose Tools | Templates.
        Behavior new_behavior = this.behavior.update(timer);
        if (new_behavior != this.behavior) {
            this.behavior = new_behavior; // When the behbavior returns null or itself.
        }
    }
    
    
    public void setHeadingEast() {
        this.setImage(this.east);
    }

    public void setHeadingWest() {
        this.setImage(this.west);
    }

    public void setHeadingSouth() {
        this.setImage(this.south);
    }

    public void setHeadingNorth() {
        this.setImage(this.north);
    }
    
    
}
