/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import java.awt.image.BufferedImage;
import utils.Assets;
import minicity.Sprite;
import minicity.Timer;

/**
 *
 * @author lukasz
 */
public class Fields extends Sprite
{
    
    public Fields() {
        super(Assets.getImage("/terrain/fields_0.png"));
    }

    float grow_time = 0;
    boolean are_mature;
    
    
    @Override
    public void update(Timer timer) {
        super.update(timer); //To change body of generated methods, choose Tools | Templates.
        this.grow_time += timer.getDelta();
            
        if (this.grow_time < 4) {
            this.setImage(Assets.getImage("/terrain/fields_0.png"));
        } else if (this.grow_time >= 4 && this.grow_time < 8) {
            this.setImage(Assets.getImage("/terrain/fields_1.png"));
        } else {
            this.setImage(Assets.getImage("/terrain/fields_2.png"));
            this.are_mature = true;
        }
        
    }

    boolean areMature() {
        return this.are_mature;
    }

    public void setFresh() {
        this.are_mature = false;
        this.grow_time = 0;
        this.setImage(Assets.getImage("/terrain/fields_0.png"));
    }
    
    
    
}
