/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import java.awt.image.BufferedImage;
import terrain.Grid;
import terrain.TileRect;
import minicity.AnimatedSprite;
import minicity.Globals;
import minicity.Sprite;
/**
 *
 * @author lukasz
 */
public class Building extends Sprite
{

    public Building(BufferedImage image, Zone zone, int population ) {
        super(image);
        this.zone = zone;
        this.population = population;
    }

    public Zone zone;
    private int population;
    
    public TileRect rect; //<-- The grid rectangle this placeable is occupying.

    public void setRect(TileRect rect) {
        this.rect = rect;
    }
    
    public void onPlace(){
        if (this.zone.type == ZoneType.Residental2X2 || this.zone.type == ZoneType.Residental3X3) {
            Globals.population += this.population;
        }
    };

    public Building copy() {
        return null;
    }
    
}
