/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

/**
 *
 * @author lukasz
 */
public enum Size {
    SIZE2X2(0.5f, 0.5f, 0,0, 2, 2),
    SIZE3X3(1.0f, 1.0f, 0,0, 3, 3),
    SIZE4X4(1.5f, 1.5f, 0,0, 4, 4),
    SIZE5X5(0.0f, 0.0f, 0,0, 5, 5);
    
    
    Size (float offset_x_sprite, float offset_y_sprite, int offset_x, int offset_y, int size_x, int size_y) {
        this.offset_x_sprite = offset_x_sprite;
        this.offset_y_sprite = offset_y_sprite;
        this.offset_x = offset_x;
        this.offset_y = offset_y;
        this.size_x = size_x;
        this.size_y = size_y;
    }
    
    // Offsets using in placement and zoning mode.
    public float offset_x_sprite, offset_y_sprite;
    public int   offset_x       , offset_y;
    public int   size_x         , size_y;
}
