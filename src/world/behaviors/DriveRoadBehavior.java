/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world.behaviors;

import terrain.Grid;
import utils.Vector3D;
import minicity.Pathfinder;
import minicity.Timer;
import terrain.Tile;
import world.Car;

/**
 *
 * @author lukasz
 */
public class DriveRoadBehavior extends Behavior 
{
    public DriveRoadBehavior(Car villager, Pathfinder.Path path, Behavior next) {
        super(villager);
        this.path = path;
        this.next_behavior = next;
    }
    
    
    // Walking logic fields:
    Pathfinder.Path path;
    
    float t = 0; 
    int   i = 1; // The next grid index.
    Tile  current_tile;
    Tile  target_grid;
    
    Behavior next_behavior;
    
    
    public void startWalk(Pathfinder.Path path) {
        this.current_tile = null;
        this.t = 0;
        this.i = 1;
    }
    
    public Behavior update(Timer timer) {
        if (this.current_tile == null) {
            this.current_tile = car.tile;
            this.target_grid = this.path.get(i);
        }
        if (this.target_grid.x > this.current_tile.x  ) {
            car.setHeadingEast();
        } else if (this.target_grid.x < this.current_tile.x) {
            car.setHeadingWest();
        } else if (this.target_grid.y > this.current_tile.y) {
            car.setHeadingSouth();
        } else if (this.target_grid.y < this.current_tile.y){
            car.setHeadingNorth();
        }

        Vector3D current = new Vector3D(this.current_tile.x, this.current_tile.y, 0);
        Vector3D target = new Vector3D(this.target_grid.x, this.target_grid.y, 0);
        
        this.t += 0.04f;
        if (this.t < 1) {
            Vector3D location = current.lerp(t, target);
            car.setLocation(location);
        }
        if (t > 1 ) {
            if (this.i < path.size() - 1){
                this.i += 1;
                this.current_tile = path.get(i - 1);
                this.target_grid = path.get(i);
                this.car.tile = this.current_tile;
                this.t = 0;
                
            }
        }
        else if (this.i >= path.size() - 1) {
            return this.next_behavior;
        } 
        return null;
    }
}
