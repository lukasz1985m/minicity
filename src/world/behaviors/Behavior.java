/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world.behaviors;

import minicity.Timer;
import world.Car;

/**
 *
 * @author lukasz
 */
public abstract class Behavior {

    Car car;

    public Behavior (Car villager) {
        this.car = villager;
    }
    
    public abstract Behavior update(Timer t);
    
}
