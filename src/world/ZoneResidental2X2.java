/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import java.awt.image.BufferedImage;
import static minicity.Globals.commercial_demmand;
import static minicity.Globals.industrial_demmand;
import static minicity.Globals.residential_demmand;
import utils.Assets;

/**
 *
 * @author lukasz
 */
public class ZoneResidental2X2 extends Zone
{
    
    public ZoneResidental2X2() {
        super(ZoneType.Residental2X2);
    }
    
    @Override
    protected boolean canSpawn() {
        if (residential_demmand > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void drawDemmand() {
        residential_demmand -= 10;
        industrial_demmand  += 5;
        commercial_demmand  += 5;
    }
}
