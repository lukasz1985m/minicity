/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import static minicity.Globals.commercial_demmand;
import static minicity.Globals.industrial_demmand;
import static minicity.Globals.residential_demmand;

/**
 *
 * @author lukasz
 */
public class ZoneIndustrial3X3 extends Zone
{

    public ZoneIndustrial3X3() {
        super(ZoneType.Industrial3X3);
    }
    
    @Override
    protected boolean canSpawn() {
        if (industrial_demmand > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void drawDemmand() {
        industrial_demmand  -= 20;
        commercial_demmand  += 10;
        residential_demmand += 10;
    }
}
