/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import java.awt.image.BufferedImage;
import minicity.Sprite;
import minicity.Timer;
import utils.Utils;
import static minicity.Globals.*;

/**
 *
 * @author lukasz
 */
public abstract class Zone extends Sprite
{
    
    
    public Zone(ZoneType type) {
        super(type.zoning.getImage());
        this.type = type;
    }

    Size size;
    float time;
    public ZoneType type;
    
    boolean placed = false;
    boolean building_spawned = false;

    public void setPlaced(boolean placed) {
        this.placed = placed;
    }
    
    @Override
    public void update(Timer timer) {
        super.update(timer); //To change body of generated methods, choose Tools | Templates.
        if (this.placed) {
            this.time += timer.getDelta();
            int randint = Utils.randint(2, 11);
            if (!this.building_spawned && this.time > randint) {
                if (this.canSpawn()) {
                    this.spawnBuilding();
                    this.building_spawned = true;
                    this.drawDemmand();
                } else {
                    this.time = 0;
                }
            }
            
        }
    }
    
    
    protected void spawnBuilding() {
        Building building = BuildingsRepo.getBuilding(this);
        world.removeSprite(this);
        world.addSprite(building);
        building.setLocation(this.getLocation());
        building.onPlace();
    }
    
    protected abstract boolean canSpawn();
    
    protected abstract void drawDemmand();
}
