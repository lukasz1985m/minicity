/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;
import static minicity.Globals.*;

/**
 *
 * @author lukasz
 */
public class ZoneIndustrial2X2 extends Zone
{
    public ZoneIndustrial2X2() {
        super(ZoneType.Industrial2X2);
    }
    
    
    @Override
    protected boolean canSpawn() {
        if (industrial_demmand > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void drawDemmand() {
        industrial_demmand  -= 10;
        commercial_demmand  += 5;
        residential_demmand += 5;
    }
    
    
}
