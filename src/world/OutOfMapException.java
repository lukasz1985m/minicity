/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

/**
 *
 * @author lukasz
 */
public class OutOfMapException extends RuntimeException
{

    public OutOfMapException(int x, int y) {
        super("Out of map exception: " + x + " " + y);
    }
    
}
