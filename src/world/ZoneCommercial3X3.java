/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import java.awt.image.BufferedImage;
import utils.Assets;
import static minicity.Globals.*;

/**
 *
 * @author lukasz
 */
public class ZoneCommercial3X3 extends Zone
{
    
    public ZoneCommercial3X3() {
        super(ZoneType.Commercial3X3);
    }
    
    @Override
    protected boolean canSpawn() {
        if (commercial_demmand > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void drawDemmand() {
        commercial_demmand  -= 20;
        industrial_demmand  += 10;
        residential_demmand += 10;
    }
}
