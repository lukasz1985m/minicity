/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import utils.Assets;
import utils.Utils;
        

/**
 *
 * @author lukasz
 */
public class BuildingsRepo {
    
    private static class BuildingInfo{
        public BuildingInfo(String image, int population) {
            this.image = Assets.getImage(image);
            this.population = population;
        }
        
        BufferedImage image;
        int population;
    }
    
    static ArrayList<BuildingInfo> buildings_residental_small = new ArrayList<BuildingInfo>(){{
        add(new BuildingInfo("/buildings/residental_1.png", 10));
        add(new BuildingInfo("/buildings/residental_2.png", 10));
        add(new BuildingInfo("/buildings/residental_3.png", 10));
        add(new BuildingInfo("/buildings/residental_4.png", 10));
    }};
    
    
    static ArrayList<BuildingInfo> buildings_residental_big = new ArrayList<BuildingInfo>(){{
        add(new BuildingInfo("/buildings/residental_big_1.png", 100));
        add(new BuildingInfo("/buildings/residental_big_2.png", 100));
        add(new BuildingInfo("/buildings/residental_big_3.png", 100));
        add(new BuildingInfo("/buildings/residental_big_4.png", 100));
    }};
    
  
    static ArrayList<BuildingInfo> buildings_industrial_small = new ArrayList<BuildingInfo>(){{
        add(new BuildingInfo("/buildings/industrial_1.png", 0));
        add(new BuildingInfo("/buildings/industrial_2.png", 0));

    }};
    
    static ArrayList<BuildingInfo> buildings_industrial_big = new ArrayList<BuildingInfo>(){{
        add(new BuildingInfo("/buildings/industrial_big_1.png", 0));
        add(new BuildingInfo("/buildings/industrial_big_2.png", 0));
        add(new BuildingInfo("/buildings/industrial_big_3.png", 0));
        add(new BuildingInfo("/buildings/industrial_big_4.png", 0));
    }};
    
  
    
    public static Building getBuilding(Zone zone) {
        ArrayList<BuildingInfo> possible_buildings = null;
        
        if (zone.type == ZoneType.Residental2X2) {
            possible_buildings = buildings_residental_small;
        }
        
        if (zone.type == ZoneType.Residental3X3) {
            possible_buildings = buildings_residental_big;
        }
        if (zone.type == ZoneType.Industrial2X2) {
            possible_buildings = buildings_industrial_small;
        }
        if (zone.type == ZoneType.Industrial3X3) {
            possible_buildings = buildings_industrial_big;
        }
        
        int variation = Utils.randint(1, possible_buildings.size());
        int index = variation - 1;
        BuildingInfo info = possible_buildings.get(index);
        return new Building(info.image, zone, info.population);
    }
}
