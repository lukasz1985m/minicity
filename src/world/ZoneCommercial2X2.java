/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import utils.Assets;

import static minicity.Globals.*;

/**
 *
 * @author lukasz
 */
public class ZoneCommercial2X2 extends Zone
{

    public ZoneCommercial2X2() {
        super(ZoneType.Commercial2X2);
    }

    @Override
    protected boolean canSpawn() {
        if (commercial_demmand > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void drawDemmand() {
        commercial_demmand  -= 10;
        industrial_demmand  += 5;
        residential_demmand += 5;
    }
    
    
    
    
}
