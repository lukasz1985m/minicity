/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import static minicity.Globals.commercial_demmand;
import static minicity.Globals.industrial_demmand;
import static minicity.Globals.residential_demmand;
import utils.Assets;

/**
 *
 * @author lukasz
 */
public class ZoneResidental3X3 extends Zone
{
     public ZoneResidental3X3() {
        super(ZoneType.Residental3X3);
    }
     
     
     @Override
    protected boolean canSpawn() {
        if (residential_demmand > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void drawDemmand() {
        residential_demmand -= 20;
        industrial_demmand  += 10;
        commercial_demmand  += 10;
    }
}
