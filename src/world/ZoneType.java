/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package world;

import minicity.Sprite;
import utils.Assets;

/**
 *
 * @author lukasz
 */
public enum ZoneType {
        Residental2X2(Size.SIZE2X2, Assets.getSprite("/zoning/residental2x2.png")), 
        Residental3X3(Size.SIZE3X3, Assets.getSprite("/zoning/residental3x3.png")), 
        Residental4X4(Size.SIZE4X4, Assets.getSprite("/zoning/residental4x4.png")),
        
        Commercial2X2(Size.SIZE2X2, Assets.getSprite("/zoning/commercial2x2.png")), 
        Commercial3X3(Size.SIZE3X3, Assets.getSprite("/zoning/commercial3x3.png")), 
        Commercial4X4(Size.SIZE4X4, Assets.getSprite("/zoning/commercial4x4.png")),
        
        Industrial2X2(Size.SIZE2X2, Assets.getSprite("/zoning/industrial2x2.png")), 
        Industrial3X3(Size.SIZE3X3, Assets.getSprite("/zoning/industrial3x3.png")), 
        Industrial4X4(Size.SIZE4X4, Assets.getSprite("/zoning/industrial4x4.png"));
        
        ZoneType(Size size, Sprite zone) {
            this.size = size;
            this.zoning = zone;
        }
        
        public Size   size;   // Size of the zone
        public Sprite zoning; // The sprite to use for highlighting a zoning action
        
        public Size getSize() {
            return this.size;
        }
        
        
        public Sprite getZoningSprite() {
            return this.zoning;
        }
        
        public Zone createZone () {
            switch(this) {
                case Residental2X2: return new ZoneResidental2X2();
                case Residental3X3: return new ZoneResidental3X3();
                case Commercial2X2: return new ZoneCommercial2X2();
                case Commercial3X3: return new ZoneCommercial3X3();
                case Industrial2X2: return new ZoneIndustrial2X2();
                case Industrial3X3: return new ZoneIndustrial3X3();
            }
            
            return null;
        }
        
}
