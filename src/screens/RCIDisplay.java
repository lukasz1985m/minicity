/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import gui.Widget;
import java.awt.Color;
import java.awt.Graphics2D;
import static minicity.Globals.*;
import utils.IntVector2D;
/**
 *
 * @author lukasz
 */
public class RCIDisplay extends Widget
{

    @Override
    public void draw(Graphics2D g2d) {
        // Draw horizontal line
        IntVector2D pos = this.getPosition();
        
        
        g2d.setColor(Color.BLACK);
        g2d.drawLine(pos.x, pos.y + 20, pos.x + 60 ,pos.y + 20);
        
        float div = 4;
        
        if (residential_demmand != 0) {
            float h = residential_demmand / div;
            g2d.setColor(Color.GREEN);
            g2d.fillRect(pos.x, (int) (pos.y + 20 - h), 20, (int)h);
        }
        
        if (commercial_demmand != 0) {
            float h = commercial_demmand / div;
            g2d.setColor(Color.BLUE);
            g2d.fillRect(pos.x + 20 , (int) (pos.y + 20 - h), 20, (int)h);
        }
        
        if (industrial_demmand != 0) {
            float h = industrial_demmand / div;
            g2d.setColor(Color.YELLOW);
            g2d.fillRect(pos.x + 40 , (int) (pos.y + 20 - h), 20, (int)h);
        }
        
        
    }
    
}
