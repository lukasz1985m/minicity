/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import gui.Box;
import gui.Button;
import gui.Container;
import gui.Text;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import utils.Assets;
import minicity.Globals;
import static minicity.Globals.*;
import world.ZoneType;

/**
 *
 * @author lukasz
 */
public class InGameScreen extends Screen 
{


    public InGameScreen() {
        this.createButtos();
    }

    
    Container buttons_container;

    // Utilities buttons
    Button build_path_mode_button;
    Button remove_path_mode_button;
    Button cut_trees_button;
    
    // Zoning buttons
    Button zone_residental_2x2;
    Button zone_residental_3x3;
    Button zone_commercial_2x2;
    Button zone_commercial_3x3;
    Button zone_industrial_2x2;
    Button zone_industrial_3x3;

    Text population;
    
    private void createButtos() {
        
        this.buttons_container = new Container();
        this.buttons_container.setBackground(new Box(255,255,255));
        this.buttons_container.setDimensions(100, 290);

        int y = 10;
        
        BufferedImage build_path = Assets.getImage("/gui/build_road.png");
        this.build_path_mode_button = new Button(build_path){
            @Override
            public void onMouseOver(MouseEvent e) {
                super.onMouseOver(e); //To change body of generated methods, choose Tools | Templates.
                status_bar.displayMessage("Build paths around the map.");
            }

            @Override
            public void onMouseOut(MouseEvent e) {
                status_bar.revert();
            }
            
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                game.setMode(build_path_mode);
                return true;
            }
        };
        this.build_path_mode_button.setPosition(10, y);
        this.buttons_container.add(this.build_path_mode_button);
        
        BufferedImage remove_road = Assets.getImage("/gui/remove_road.png");
        this.remove_path_mode_button = new Button(remove_road) {
            @Override
            public void onMouseOver(MouseEvent e) {
                super.onMouseOver(e); //To change body of generated methods, choose Tools | Templates.
                status_bar.displayMessage("Remove roads from map");
            }

            @Override
            public void onMouseOut(MouseEvent e) {
                super.onMouseOut(e); //To change body of generated methods, choose Tools | Templates.
                status_bar.revert();
            }

            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                game.setMode(remove_path_mode);
                return true;
            }
        };
        this.remove_path_mode_button.setPosition(50, y);
        this.buttons_container.add(this.remove_path_mode_button);
        
        y += 40;
        
        this.cut_trees_button = new Button("/gui/cut_trees.png") {
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e); //To change body of generated methods, choose Tools | Templates.
                game.setMode(cut_trees_mode);
                return true;
            }
        } ;
        this.cut_trees_button.setPosition(10, y);
        this.buttons_container.add(cut_trees_button);
        
        y += 40;
        
        this.zone_residental_2x2 = new Button("/gui/residental_small.png"){
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                zoning_mode.setType(ZoneType.Residental2X2);
                game.setMode(zoning_mode);
                return true;
            }
        };
        this.zone_residental_2x2.setPosition(10, y);
        this.buttons_container.add(this.zone_residental_2x2);
        
        this.zone_residental_3x3 = new Button("/gui/residental_big.png"){
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                zoning_mode.setType(ZoneType.Residental3X3);
                game.setMode(zoning_mode);
                return true;
            }
        };
        this.zone_residental_3x3.setPosition(50, y);
        this.buttons_container.add(this.zone_residental_3x3);
        
        y += 40;
        
        this.zone_commercial_2x2 = new Button("/gui/commercial_small.png"){
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                zoning_mode.setType(ZoneType.Commercial2X2);
                game.setMode(zoning_mode);
                return true;
            }
        };
        this.zone_commercial_2x2.setPosition(10, y);
        this.buttons_container.add(this.zone_commercial_2x2);
        
        this.zone_commercial_3x3 = new Button("/gui/commercial_big.png"){
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                zoning_mode.setType(ZoneType.Commercial3X3);
                game.setMode(zoning_mode);
                return true;
            }
        };
        this.zone_commercial_3x3.setPosition(50, y);
        this.buttons_container.add(this.zone_commercial_3x3);
        
        y += 40;
        
        this.zone_industrial_2x2 = new Button("/gui/industrial_small.png"){
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                zoning_mode.setType(ZoneType.Industrial2X2);
                game.setMode(zoning_mode);
                return true;
            }
        };
        this.zone_industrial_2x2.setPosition(10, y);
        this.buttons_container.add(this.zone_industrial_2x2);
        
        
        this.zone_industrial_3x3 = new Button("/gui/industrial_big.png"){
            @Override
            public boolean mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                zoning_mode.setType(ZoneType.Industrial3X3);
                game.setMode(zoning_mode);
                return true;
            }
        };
        this.zone_industrial_3x3.setPosition(50, y);
        this.buttons_container.add(this.zone_industrial_3x3);
        
        
        y += 40;

        
        // RCI display:
        RCIDisplay rci = new RCIDisplay();
        rci.setPosition(17, y);
        this.buttons_container.add(rci);
        
        y += 40;

        
        // Population meter:
        this.population = new Text("Population: 0") {
            @Override
            public String getContents() {
                return "Population: " + Globals.population;
            }
        };
        this.population.setPosition(10, y);
        this.population.setFont(Assets.loadFont("/fonts/arial_14.fnt"));
        this.buttons_container.add(this.population);
        
        
        this.buttons_container.setPosition(20, 60);
    }
    
    
    @Override
    public void display() {
        gui.add(status_bar);
        gui.add(this.buttons_container);
    }

    @Override
    public void hide() {
        gui.remove(status_bar);
        gui.remove(this.buttons_container);
    }
    
}
