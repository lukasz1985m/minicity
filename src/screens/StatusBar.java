/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package screens;

import gui.Box;
import gui.Container;
import gui.Font;
import gui.Text;
import gui.Widget;
import java.awt.Dimension;
import utils.Assets;
import static minicity.Globals.viewport;

/**
 *
 * @author lukasz
 */
public class StatusBar extends Container 
{

    public StatusBar () {
        Dimension size = viewport.getSize();
        
        this.background = new Box();
        this.background.setColor(255, 255, 255, 255);
        this.background.setDimensions(size.width, 30);
        this.add(this.background);
        
        this.text = new Text("Status bar");
        Font font = Assets.loadFont("/fonts/arial_14.fnt");
        this.text.setFont(font);
        this.text.setPosition(5, 0);
        this.add(this.text);
        
        this.setPosition(0, size.height - 16);
        
    }
    
    private final Text text;
    private final Box background;
    
    
    public void displayMessage(String message) {
        this.text.setContents(message);
    }
    
    
    public void revert() {
        this.text.setContents("Status bar");
    }
    
}
