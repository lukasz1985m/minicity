/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import java.awt.image.BufferedImage;

/**
 *
 * @author lukasz
 */
public class AnimatedSprite extends Sprite
{
    
    public AnimatedSprite(BufferedImage[] frames) {
        super(frames[0]);
        this.frames = frames;
    }

    BufferedImage[] frames;
    float play_time;
    
    @Override
    public void update(Timer timer) {
        super.update(timer); //To change body of generated methods, choose Tools | Templates.
        this.play_time += timer.delta;
        
        // Playback at 4 fps:
        int frame = (int)(this.play_time * 4);
        
        this.setImage(this.frames[frame % this.frames.length]);
    }
    
    public void setAnimation(BufferedImage[] frames) {
        this.frames = frames;
    }
    
}
