/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import input.MouseHandler;
import utils.Vector3D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import static minicity.Globals.input;
import static minicity.Globals.viewport;

/**
 *
 * @author lukasz
 */
public class Scroller implements MouseHandler{

    Vector3D offset = new Vector3D();

    boolean enabled = true;
    
    public boolean mouseMoved(MouseEvent me) {
        int x = me.getX();
        int y = me.getY();
        
        Viewport viewport = Globals.viewport;
        Rectangle bounds  = viewport.getBounds();

        int margin = 20;
        float magnitude = 0.5f;
        
        // Mouse on corners
        if (x < margin && y < margin) {
            this.offset.set(-magnitude, 0, 0); // Mouse in top-left corner
            return true;
        }
        else if (x > bounds.width - margin && y < margin) {
            this.offset.set(0, -magnitude, 0); // Mouse in top-right corner
            return true;
        }
        else if (x < margin && y > bounds.height - margin){
            this.offset.set(0, magnitude, 0); // Mouse in btm-left corner
            return true;
        }
        else if(x > bounds.width - margin &&  y > bounds.height - margin){
            this.offset.set(magnitude, 0, 0); // Mouse in btm-right corner
            return true;
        }
        // Mouse on sides
        else if(x < margin) {
            this.offset.set(-magnitude, magnitude, 0); // Mouse on left side
            return true;
        }
        else if(x > bounds.width - margin) {
            this.offset.set(magnitude, -magnitude, 0); // Mouse on right side
            return true;
        }
        else if(y < margin) {
            this.offset.set(-magnitude, -magnitude, 0); // Mouse on top side.
            return true;
        } 
        else if(y > bounds.height - margin) {
            this.offset.set(magnitude, magnitude, 0); // Mouse on bottom side.
            return true;
        }
        else {
            this.offset.set(0, 0, 0);
        }
        return false;
    }
    
    
    public void enable() {
        this.enabled = true;
        input.addMouseHandler(this);
    }
    
    
    public void disable() {
        this.enabled = false;
        input.removeMouseHandler(this);

    }
    
    public void update() {
        if(this.enabled) {
            
            Vector3D center = viewport.getCenter();
            Vector3D new_center = new Vector3D(center);
            new_center.add(this.offset);
            viewport.setCenter(new_center);
        }
    }

    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mousePressed(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseReleased(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseEntered(MouseEvent e) {
        return false;
    }

    @Override
    public boolean mouseExited(MouseEvent e) {
        this.offset.set(0, 0, 0);
        return false;
    }
    
    @Override
    public boolean mouseWheelMoved(MouseWheelEvent e) {
        int rot = e.getWheelRotation();
        if (rot > 0) {
            viewport.zoomOut();
        }
        else {
            viewport.zoomIn();
        }
        return false;
    }
}
