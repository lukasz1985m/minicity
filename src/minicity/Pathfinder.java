/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import terrain.Grid;
import static minicity.Globals.terrain;
import terrain.RoadTile;
import terrain.Tile;
import world.Building;

/**
 *
 * @author lukasz
 */
public class Pathfinder 
{
    public static abstract class Condition
    {
        public abstract boolean meets(Tile grid);
    }
    
    
    public static class Path extends ArrayList<Tile>
    {
        
        public Path() {
            super();
        }
        
        public Path(Path path) {
            super(path);
        }
        
        public Path reversed() {
            Path reversed = new Path(this);
            Collections.reverse(reversed);
            return reversed;
        }
        
        @Override
        public String toString() {
            String str = "";
            for (Tile grid : this) {
                str += grid.toString();
            }
            return str;
        }
        
    }
    
    
    public static Path findPath(Tile start, Condition end_condition) {
        ArrayList<Tile> frontier = new ArrayList<>();
        frontier.add(start);
        
        HashMap<Tile, Tile> came_from = new HashMap<>();
        came_from.put(start, null);

        Tile current = null;
        boolean has_found = false;
        
        while(!frontier.isEmpty()){
            current = frontier.get(0);
            frontier.remove(current);
            
            if (end_condition.meets(current)) {
                has_found = true;
                break;
            }
            
            ArrayList<Tile> neighbors = new ArrayList<>();
            
            int x = current.x;
            int y = current.y;
            
             try{
                Tile grid_west = terrain.getTile(x - 1, y); 
                if (grid_west.containsObject(RoadTile.class)) {
                    neighbors.add(grid_west);
                }
            } catch (ArrayIndexOutOfBoundsException e) {}
            
            try{
                Tile grid_east = terrain.getTile(x + 1, y); 
                if (grid_east.containsObject(RoadTile.class)) {
                    neighbors.add(grid_east);
                }
            } catch(ArrayIndexOutOfBoundsException e) {}
            
            try{
                Tile grid_north = terrain.getTile(x , y - 1); 
                if (grid_north.containsObject(RoadTile.class)) {
                    neighbors.add(grid_north);
                }
            } catch (ArrayIndexOutOfBoundsException e) {}
            
            try{
                Tile grid_south = terrain.getTile(x , y + 1); 
                if (grid_south.containsObject(RoadTile.class)) {
                    neighbors.add(grid_south);
                }
            } catch (ArrayIndexOutOfBoundsException e){}
            
            
            for (Tile next : neighbors) {
                if(came_from.get(next) == null) {
                    if (next != null) {
                        frontier.add(next);
                        came_from.put(next, current);
                    }
                }
            }
            
        }
        
        
        if (has_found) {
            Path path = new Path();
            path.add(current);
            
            while(current != start) {
                current = came_from.get(current);
                path.add(current);
            }
            Collections.reverse(path);
            return path;
        }
        return null;
    }
    
    
    public static Path findPath(Tile start, final Tile target) {
        Path path = findPath(start, new Condition(){
            @Override
            public boolean meets(Tile grid) {
                if (grid == target) {
                    return true;
                } 
                else {
                    return false;
                }
            }
        
        });
        
        return path;
    }
}
