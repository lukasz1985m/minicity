/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

/**
 *
 * @author lukasz
 */
public class Timer 
{
    
    float seconds;
    float delta;
    
    public void update() {
        float seconds = System.nanoTime() / 1_000_000_000f;
        this.delta = seconds - this.seconds;
        this.seconds = seconds;
        
    }

    public float getDelta() {
        return delta;
    }
    
        
}
