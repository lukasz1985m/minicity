/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author lukasz
 */
public class Scene {
    
    public HashSet<Sprite> sprites = new HashSet<>();
    
    public void addSprite(Sprite sprite) {
        HashSet<Sprite> sprites = new HashSet<>();
        sprites.addAll(this.sprites);
        sprites.add(sprite);
        this.sprites = sprites;
    }

    public void removeSprite(Sprite sprite) {
        HashSet<Sprite> sprites = new HashSet<>();
        sprites.addAll(this.sprites);
        sprites.remove(sprite);
        this.sprites = sprites;
    }
    
    public HashSet<Sprite> getSprites() {
        return sprites;
    }
    
    public void update(Timer timer) {
        
        for (Sprite sprite : this.sprites) {
            sprite.update(timer);
        }
        
    }
    
    
}
