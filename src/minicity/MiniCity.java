/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import screens.StatusBar;
import input.Input;
import utils.Assets;
import terrain.Land;
import gui.Box;
import gui.Button;
import gui.Container;
import gui.Font;
import gui.Gui;
import gui.Text;
import gui.TextField;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import screens.Screen;
import terrain.GroundTile;
import terrain.SimplexNoiseHeightmap;
import terrain.Terrain;
import static minicity.Globals.*;
import world.ZoneType;

/**
 *
 * @author lukasz
 */
public class MiniCity implements ActionListener
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MiniCity game = new MiniCity();
        game.start();
    }
    
    private Timer timer = new Timer();
    
    public MiniCity() {}
    
    
    /**
     * The initialization part of the game. 
     */
    public void start(){
        game = this;
        
        window   = new JFrame("Village");
        viewport = new Viewport();
        
        window.add(Globals.viewport);
        
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
        viewport.initialize();
        
        input = new Input();    
        viewport.addMouseListener(input);
        viewport.addMouseMotionListener(input);
        viewport.addKeyListener(input);
        viewport.addMouseWheelListener(input);
        
        gui        = new Gui();
        input.addKeyHandler(gui);
        input.addMouseHandler(gui);
        
        world = new Scene();
        land  = new Land();
        
        scroller = new Scroller();
        
        // Testing code:
        SimplexNoiseHeightmap hm = new SimplexNoiseHeightmap(60, 60, 0, 40);
        terrain = new Terrain(hm);
        terrain.create();
//        GroundTile tile = new GroundTile(Assets.loadImage("/terrain/grass.png"));
//        tile.setLocation(10, 0, 0);
//        land.addSprite(tile);
        
        
//        this.setMode(select_mode);
//        this.setMode(placement_mode);
//      
    
        zoning_mode.setType(ZoneType.Residental2X2);
        this.setMode(zoning_mode);
        
//        this.setMode(cut_trees_mode);

        status_bar = new StatusBar();
        
        
        
        this.setScreen(in_game_screen);
        
//        Sprite sprite = Assets.getSprite("/zoning/residental2x2.png");
//        world.addSprite(sprite);
//        Pathfinder.Path path = Pathfinder.findPath(terrain.getTile(0, 0), terrain.getTile(3, 1));
        
//        System.out.println("Path is " + path);
        
        /*
        Box box = new Box();
        box.setDimensions(100, 100);
        box.setPosition(50, 50);
        gui.add(box);
        Box box2 = new Box(new Color(255,0,0));
        box2.setDimensions(100, 100);
        box2.setPosition(60, 60);
        gui.add(box2);
        Container cnt = new Container();
        gui.add(cnt);
        Box box3 = new Box(new Color(255,255,0));
        box3.setPosition(0, 0);
        box3.setDimensions(40, 100);
        cnt.add(box3);
        Box box4 = new Box(new Color(255,0,255));
        box4.setPosition(60, 0);
        box4.setDimensions(40, 100);
        cnt.add(box4);
        cnt.setPosition(200, 200);
        Font arial = Assets.loadFont("/gui/arial.fnt");
        Text text = new Text("Hello world");
        text.setFont(arial);
        text.setPosition(100, 200);
        gui.add(text);
        TextField field = new TextField("textfield");
        field.setFont(arial);
        field.setPosition(100, 300);
        gui.add(field);
         */
        
        
        
        // Run loop:
        scroller.enable();

        
        javax.swing.Timer timer = new javax.swing.Timer(16, this);
        timer.start();
    }

    
    public void setMode(GameMode new_mode) {
        current_mode.disable();
        current_mode = new_mode;
        current_mode.enable();
    }
    
    public void setScreen(Screen screen) {
        current_screen.hide();
        current_screen = screen;
        current_screen.display();
    }
    
    
    /** Action performed is an event that occurs on timer tick */
    public void actionPerformed(ActionEvent ae) {
        this.timer.update();
//        System.out.println(this.timer.delta);
        viewport.draw();
        current_mode.update(this.timer);
        land.update(this.timer);
        world.update(this.timer);
        scroller.update();  
    }

   
    
}
