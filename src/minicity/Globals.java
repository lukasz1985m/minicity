/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import screens.StatusBar;
import modes.BuildRoadMode;
import modes.PlacementMode;
import modes.RemoveRoadMode;
import modes.SelectMode;
import input.Input;
import terrain.Land;
import gui.Gui;
import javax.swing.JFrame;
import modes.CutTreesMode;
import modes.ZoningMode;
import screens.InGameScreen;
import screens.Screen;
import terrain.Terrain;

/**
 *
 * @author lukasz
 */
public class Globals {
    
    public static MiniCity  game;
    
    public static JFrame    window;
    public static Viewport  viewport;
    
    public static Input     input;  
    
    public static Gui       gui;
    public static StatusBar status_bar;
    
    public static Scroller  scroller;
    public static Scene     world;
    public static Scene     land;
    public static Terrain   terrain;    
    
    
    
    // The current game mode:
    public static GameMode  current_mode = new GameMode();
    
    // All The possible game modes:
    public static SelectMode        select_mode         = new SelectMode();
    public static BuildRoadMode     build_path_mode     = new BuildRoadMode();
    public static RemoveRoadMode    remove_path_mode    = new RemoveRoadMode();
    public static ZoningMode        zoning_mode         = new ZoningMode();
    public static CutTreesMode      cut_trees_mode      = new CutTreesMode();
    public static PlacementMode     placement_mode      = new PlacementMode();
    
    // Demmands
    public static int residential_demmand = 140;
    public static int commercial_demmand = 20;
    public static int industrial_demmand = 40;
    
    // Population:
    public static int population = 0;
    
    // The current screen:
    public static Screen current_screen = new Screen();
    
    public static InGameScreen in_game_screen = new InGameScreen();
    
}
