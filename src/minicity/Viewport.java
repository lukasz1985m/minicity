/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import utils.Vector2D;
import utils.Vector3D;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import terrain.Grid;
import terrain.Tile;
import static minicity.Globals.gui;

/**
 *
 * @author lukasz
 */
public class Viewport extends Canvas {

    public Viewport(){
        this.setPreferredSize(new Dimension(800, 600));
    }
    
    private Vector3D center = new Vector3D();
    
    private BufferStrategy strategy;

    ArrayList<Sprite> scene_sprites   = new ArrayList<>();
    ArrayList<Sprite> land_sprites = new ArrayList<>();
    
    private int zoom_level = 2;
    
    public void zoomIn() {
        this.zoom_level += 1;
        if (this.zoom_level > 2) {
            this.zoom_level = 2;
        }
    }
    
    public void zoomOut() {
        this.zoom_level -= 1;
        if (this.zoom_level < 1) {
            this.zoom_level = 1;
        }
    }
    
    public int getZoomLevel() {
        return this.zoom_level;
    }
    
    public Vector3D getCenter() {
        return center;
    }

    public void setCenter(Vector3D center) {
        this.center = center;
    }

    public void initialize() {
        this.createBufferStrategy(2);
        this.strategy = this.getBufferStrategy();
    }
    
    public void draw() {
        
        Graphics2D g2d = (Graphics2D)this.strategy.getDrawGraphics();
        
        // Screen clearing:
        Rectangle bounds = this.getBounds();
        g2d.clearRect(0, 0, bounds.width, bounds.height);
        
        this.cullLand();
        this.cullOverground();
        
        this.sortLand();
        this.sortOverground();
        
        this.drawLand(g2d);
        this.drawOverground(g2d);
        this.drawGui(g2d);
        
        this.strategy.show();
    }
    
    
    private void cullLand() {
        
        Rectangle bounds = this.getBounds();
        
        this.land_sprites.clear();
        
        for (Sprite sprite : Globals.land.sprites) {
            Vector3D location = sprite.getLocation();
            Vector2D projected = this.project(location);
            if (projected.x > 0 && projected.y > 0 && projected.x < bounds.width && projected.y < bounds.height) {
                this.land_sprites.add(sprite);
            }
        }
        
    }
    
    private void cullOverground(){
        Rectangle bounds = this.getBounds();
        
        this.scene_sprites.clear();
        
        for (Sprite sprite : Globals.world.sprites) {
            Vector3D location = sprite.getLocation();
            Vector2D projected = this.project(location);
            if (projected.x > 0 && projected.y > 0 && projected.x < bounds.width && projected.y < bounds.height) {
                this.scene_sprites.add(sprite);
            }
        }
        
        
    }
    
    
    private void sortLand() {
        Collections.sort(this.land_sprites, new Comparator<Sprite>() {
            public int compare(Sprite spr1, Sprite spr2) {
                Vector3D loc1 = spr1.getLocation();
                Vector3D loc2 = spr2.getLocation();
                return Math.round(loc1.x + loc1.y + loc1.z - loc2.x - loc2.y - loc2.z);
            }
        });
    }
    
    private void sortOverground() {
        Collections.sort(this.scene_sprites, new Comparator<Sprite>() {
            public int compare(Sprite spr1, Sprite spr2) {
                Vector3D loc1 = spr1.getLocation();
                Vector3D loc2 = spr2.getLocation();
                return Math.round(loc1.x + loc1.y + loc1.z - loc2.x - loc2.y - loc2.z);
            }
        });
    }
    
    
    private void drawGui(Graphics2D g2d) {
        gui.draw(g2d);
    }
    
    private void drawLand(Graphics2D g2d) {
        for (Sprite terrain_sprite : this.land_sprites) {
            terrain_sprite.draw(g2d);
        }
    }
    
    
    private void drawOverground(Graphics2D g2d) {
        for (Sprite scene_sprite : this.scene_sprites) {
            scene_sprite.draw(g2d);
        }
    }
    
    
    public Vector2D projectLocal(Vector3D in) {
        float x = (in.x - in.y) * 16 * this.zoom_level;
        float y = (in.x + in.y - in.z) * 8 * this.zoom_level;
        
        return new Vector2D(x,y);
        
    }

    public Vector2D project(Vector3D input) {
        Vector2D center_pos = this.projectLocal(this.center);
        Vector2D input_pos  = this.projectLocal(input);
        
        Vector2D projected = Vector2D.sub(input_pos, center_pos);
        
        projected.x += this.getWidth() / 2;
        projected.y += this.getHeight() /2 ;
        
        return projected;
    }
    
    
    public ArrayList pickWorld() {
        throw new UnsupportedOperationException();
    }
    
    public Tile pickTile(int mouse_x, int mouse_y) {
        for (Sprite land_sprite : this.land_sprites) {
            Tile pick = (Tile) land_sprite.handlePick(mouse_x, mouse_y);
            if (pick != null) {
                return pick;
            }
        }
        return null;
    }

    

}
