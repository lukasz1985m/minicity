/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import java.awt.image.BufferedImage;
import terrain.Grid;
import terrain.GroundTile;
import terrain.RoadTile;
import terrain.Tile;
import utils.Assets;
import static minicity.Globals.land;
import static minicity.Globals.terrain;
import world.OutOfMapException;

/**
 *
 * @author lukasz
 */
public class RoadOperations 
{
    public static void buildRoad(Tile tile) {            
        String img_path = String.format("/roads/road_0000.png" );
        BufferedImage img = null;
        img = Assets.getImage(img_path);
        RoadTile road_tile = new RoadTile(img);
        road_tile.x = tile.x;
        road_tile.y = tile.y;
        road_tile.setLocation(tile.getLocation());
        
        terrain.setTile(tile.x, tile.y, road_tile);
        
        land.addSprite(road_tile);
        land.removeSprite(tile);
        realignRoad(road_tile);
    
        System.out.println(tile.x + " " + tile.y);

        
        try {
            Tile tile_n = terrain.getTile(tile.x, tile.y - 1);
            realignRoad(tile_n);
        } catch (OutOfMapException e) { }
        
        try {
            Tile tile_s = terrain.getTile(tile.x, tile.y + 1);
            realignRoad(tile_s);
        } catch (OutOfMapException e) { }
        
        try {
            Tile tile_w = terrain.getTile(tile.x - 1, tile.y );
            realignRoad(tile_w);
        } catch (OutOfMapException e) { }
        
        try {
            Tile tile_e = terrain.getTile(tile.x + 1, tile.y);
            realignRoad(tile_e);
        } catch (OutOfMapException e) { }
    }
    
    public static void removeRoad(Tile tile) {
        String img_terrain = String.format("/terrain/grass.png" );
        
        land.removeSprite(tile);
        
        BufferedImage img = null;
        img = Assets.getImage(img_terrain);
        GroundTile ground_tile = new GroundTile(img);
        ground_tile.x = tile.x;
        ground_tile.y = tile.y;
        terrain.setTile(tile.x, tile.y, ground_tile);
        ground_tile.setLocation(tile.getLocation());
        land.addSprite(ground_tile);
        
        
        realignRoad(ground_tile);
        
        try {
            Tile tile_n = terrain.getTile(tile.x, tile.y - 1);
            realignRoad(tile_n);
        } catch (OutOfMapException e) { }
        
        try {
            Tile tile_s = terrain.getTile(tile.x, tile.y + 1);
            realignRoad(tile_s);
        } catch (OutOfMapException e) { }
        
        try {
            Tile tile_w = terrain.getTile(tile.x - 1, tile.y );
            realignRoad(tile_w);
        } catch (OutOfMapException e) { }
        
        try {
            Tile tile_e = terrain.getTile(tile.x + 1, tile.y);
            realignRoad(tile_e);
        } catch (OutOfMapException e) { }
    }
    
    public static void realignRoad(Tile tile) {
        if (tile instanceof RoadTile) {
            int north, south, west, east;
            
            try {
                Tile tile_n = terrain.getTile(tile.x, tile.y - 1);
                if (tile_n instanceof RoadTile) {
                    north = 1; 
                } else {
                    north = 0;
                }
            }   catch (OutOfMapException e) {
                north = 0;
            }            

            try {
                Tile tile_s = terrain.getTile(tile.x, tile.y + 1);
                if (tile_s instanceof RoadTile) {
                    south = 1; 
                } else {
                    south = 0;
                }
            }   catch (OutOfMapException e) {
                south = 0;
            }            

            try {
                Tile tile_w = terrain.getTile(tile.x - 1, tile.y);
                if (tile_w instanceof RoadTile) {
                    west = 1; 
                } else {
                    west = 0;
                }
            }   catch (OutOfMapException e) {
                west = 0;
            }            

            try {
                Tile tile_e = terrain.getTile(tile.x + 1, tile.y);
                if (tile_e instanceof RoadTile) {
                    east = 1; 
                } else {
                    east = 0;
                }
            }   catch (OutOfMapException e) {
                east = 0;
            }       
            
            
            String img_path = String.format("/roads/road_%d%d%d%d.png", north, south, west, east);
            BufferedImage img = null;
            img = Assets.getImage(img_path);
            RoadTile road_tile = new RoadTile(img);
            road_tile.x = tile.x;
            road_tile.y = tile.y;
            road_tile.setLocation(tile.getLocation());
            land.removeSprite(tile);
            land.addSprite(road_tile);
            terrain.setTile(tile.x, tile.y, road_tile);
        }
        
    }
    
}
