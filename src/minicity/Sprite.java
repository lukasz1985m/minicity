/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package minicity;

import java.awt.Color;
import utils.Vector2D;
import utils.Vector3D;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import static minicity.Globals.viewport;

/**
 *
 * @author lukasz
 */
public class Sprite {
    
    public Sprite(BufferedImage image) {
        this.image = image;
    }
    
    
    private Vector3D location = new Vector3D();
    
    BufferedImage image;
    boolean visible = true;
    
    

    public Vector3D getLocation() {
        return new Vector3D(location);
    }

    public void setLocation(Vector3D location) {
        this.location = location;
    }
    
    public void setLocation(float x, float y , float z) {
        this.location = new Vector3D(x,y,z);
    }
    

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
    
    
    
    public void draw(Graphics2D g2d) {
        Vector2D position = viewport.project(this.location);
        
        int w = this.image.getWidth();
        int h = this.image.getHeight();
        if (this.visible) {
            int zoom_level = viewport.getZoomLevel();
            if (zoom_level == 1) {
                g2d.drawImage(this.image, (int)position.x - w / 2, (int)position.y - h / 2, null);
            }
            else {
                g2d.drawImage(this.image, (int)position.x - w, (int)position.y - h, w*2, h*2, null);
            }
//            g2d.drawRect((int)position.x - w / 2, (int)position.y - h / 2, w , h );
        }
    }
    
    public Sprite handlePick(int mouse_x, int mouse_y) {
        
        int zoom_level = viewport.getZoomLevel();
        
        if (zoom_level == 1) {
            Vector2D position = Globals.viewport.project(this.location);

            int w = this.image.getWidth();
            int h = this.image.getHeight();

            int sprite_x = (int) (position.x - w / 2) ;
            int sprite_y = (int) (position.y - h / 2) ;

            int rel_x = mouse_x - sprite_x;
            int rel_y = mouse_y - sprite_y;


            try {
                int clr_int = this.image.getRGB(rel_x, rel_y );
                Color color = new Color(clr_int, true);

                int alpha = color.getAlpha();
                if (alpha > 0.1f) {
                    return this;
                } else {
                    return null;    
                }
            } catch (ArrayIndexOutOfBoundsException e) {
            }
        } else if (zoom_level == 2) {
            Vector2D position = viewport.project(this.location);

            int w = this.image.getWidth();
            int h = this.image.getHeight();

            int sprite_x = (int) (position.x - w ) ;
            int sprite_y = (int) (position.y - h) ;

            int rel_x = mouse_x - sprite_x;
            int rel_y = mouse_y - sprite_y;


            try {
                int clr_int = this.image.getRGB(rel_x / 2, rel_y / 2);
                Color color = new Color(clr_int, true);

                int alpha = color.getAlpha();
                if (alpha > 0.1f) {
                    return this;
                } else {
                    return null;    
                }
            } catch (ArrayIndexOutOfBoundsException e) {

            }
            
        }
        return null;
    }

    public BufferedImage getImage() {
        return image;
    }
    
    
    
    public void update(Timer timer) {}
}
