/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author lukasz
 */
public class Vector2D {
    
    public Vector2D() {
        this.x = this.y = 0;
    }
    
    public Vector2D(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    public Vector2D(Vector2D orig) {
        this.x = orig.x;
        this.y = orig.y;
    }
    
    public float x,y;
    
    
    public Vector2D sub(Vector2D other) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }
    
//    public Vector2D add(Vector2D other) {
        
//    }
    
    public static Vector2D sub(Vector2D first, Vector2D second) {
        Vector2D result = new Vector2D(first);
        return result.sub(second);
    }
    
    
     public String toString() {
        return String.format("Vector3D(%.4f, %.4f)", this.x, this.y );
    }
}
