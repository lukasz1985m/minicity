/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import gui.Font;
import gui.Glyph;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import minicity.Sprite;

/**
 *
 * @author lukasz
 */
public class Assets {
    
    public static HashMap<String, Object> cache = new HashMap<>();
    
    public static BufferedImage getImage(String path) {
        
        String key = path;
        
        Object cached = cache.get(key);
        if (cached == null) {
            try {
                InputStream   stream = System.class.getResourceAsStream(path);
                BufferedImage image  = ImageIO.read(stream);
                cache.put(key, image);
                return image;
            }  catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
        else {
            return (BufferedImage) cache.get(key);
        }
    }
    
    
    public static Sprite getSprite(String path) {
        BufferedImage image = getImage(path);
        return new Sprite(image);
    }
    
    
    public static Font loadFont(String path) {
        InputStream stream = System.class.getResourceAsStream(path);
        
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(stream)
        );
        
        try {
            Font font = new Font();
            
            // Read the atlas file which has to be the same filename with png extension
            String atlas_path = path.replace("fnt", "png");
            InputStream atlas_stream = System.class.getResourceAsStream(atlas_path);
            BufferedImage atlas = ImageIO.read(atlas_stream);

            font.atlas = atlas;
                        
            
            // Read the glyphs of bitmap font
            String line;
            while( (line = reader.readLine()) != null) {
                if (line.startsWith("info ")) {
                    String[] segments = line.split(" ");
                    String name = segments[1].split("=")[1];
                    font.name = name;
                    
                    int size = Integer.parseInt(segments[2].split("=")[1]);
                    font.size = size;
                }
                
                if (line.startsWith("char ")) {

                    char chr = (char) Integer.parseInt(line.substring(8, 11).trim());
                    
                    int x = Integer.parseInt(line.substring(15, 18).trim());
                    int y = Integer.parseInt(line.substring(23, 27).trim());
                    
                    int width  = Integer.parseInt(line.substring(35, 39).trim());
                    int height = Integer.parseInt(line.substring(48, 52).trim());
                    
                    int x_offset = Integer.parseInt(line.substring(62, 66).trim());
                    int y_offset = Integer.parseInt(line.substring(76, 80).trim());
                    
                    int advance = Integer.parseInt(line.substring(91, 95).trim());
                    
                    Glyph glyph = new Glyph(
                        chr, 
                        x,y, width,height, 
                        x_offset,y_offset, advance
                    );
                    
                    font.addGlyph(chr, glyph);
                }
                
            }

            return font;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
