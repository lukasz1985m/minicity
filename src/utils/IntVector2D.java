/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author lukasz
 */
public class IntVector2D {
    public IntVector2D() {
        this.x = this.y = 0;
    }
    
    public IntVector2D(int  x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public IntVector2D(IntVector2D orig) {
        this.x = orig.x;
        this.y = orig.y;
    }
    
    public int x,y;
    
    public void set(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    
    public IntVector2D sub(IntVector2D other) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }
    
//    public Vector2D add(Vector2D other) {
        
//    }
    
    static IntVector2D sub(IntVector2D first, IntVector2D second) {
        IntVector2D result = new IntVector2D(first);
        return result.sub(second);
    }
    
    
     public String toString() {
        return String.format("Vector3D(%.4f, %.4f)", this.x, this.y );
    }
}
