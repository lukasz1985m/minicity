/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author lukasz
 */
public class Vector3D {

    
    
    public Vector3D() {
        this.x = this.y = this.z = 0f;
    }
    
    public Vector3D(float x, float y, float z){
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Vector3D(Vector3D other) {
        this(other.x, other.y, other.z);
    }
    
    public float x, y, z;
    
    
    public Vector3D set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        return this;
    }
    
    public Vector3D add(Vector3D other) {
        this.x += other.x;
        this.y += other.y;
        this.z += other.z;
        return this;    
    }
    

    public String toString() {
        return String.format("Vector3D(%.4f, %.4f, %.4f)", this.x, this.y, this.z);
    }
    
   
    public Vector3D lerp(float t, Vector3D other) {
        Vector3D result = new Vector3D(
            (1 - t)* this.x + t * other.x,       
            (1 - t)* this.y + t * other.y,
            (1 - t)* this.z + t * other.z
        );
        return result;
    }
    
    
}
